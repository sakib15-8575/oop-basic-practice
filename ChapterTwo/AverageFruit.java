package ChapterTwo;

public class AverageFruit {
    public static void main(String[] args) {
        double numOranges = 50.0E-1;
        double numApples = 1.0E1;
        double averageFruit = 0.0;

        averageFruit = (numApples + numOranges)/2.0;

        System.out.println("Average fruit is = " + averageFruit);
    }
}
