package ChapterFive;

public class TryInitialization {
    static int[] values = new int[10];

    static {
        System.out.println("Running Initialization Block.");
        for (int i = 0; i < values.length; ++i) {
            values[i] = (int) (100 * Math.random());
        }
    }

    void listValues() {
        System.out.println();
        for (int value : values) {
            System.out.print(" " + value);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        TryInitialization example = new TryInitialization();
        System.out.println("\nFirst Object:");
        example.listValues();

        example = new TryInitialization();
        System.out.println("\nSecond Object:");
        example.listValues();
    }
}
