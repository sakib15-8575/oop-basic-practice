package ChapterFive;

class Sphere {
    static final double PI = 3.14;
    static int count = 0;

    double radius;
    double xCenter;
    double yCenter;
    double zCenter;

    public Sphere(double theRadius, double x, double y, double z) {
        radius = theRadius;
        xCenter = x;
        yCenter = y;
        zCenter = z;
        ++count;
    }

    Sphere(double x, double y, double z) {
        radius = 1.0;
        xCenter = x;
        yCenter = y;
        zCenter = z;
        ++count;
    }

    Sphere() {
        radius = 1.0;
        xCenter = 0.0;
        yCenter = 0.0;
        zCenter = 0.0;
        ++count;
    }

    static int getCount() {
        return count;
    }

    double volume() {
        return 4.0/3.0*PI*radius*radius*radius;
    }
}
