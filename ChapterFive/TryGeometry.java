package ChapterFive;

public class TryGeometry {
    public static void main(String[] args) {
        Point start = new Point(0.0, 1.0);
        Point end = new Point(5.0, 6.0);
        System.out.println("Points created are " + start + " and " + end);

        Line line1 = new Line(start, end);
        Line line2 = new Line(0.0, 3.0, 3.0, 0.0);
        System.out.println("Lines created are " + line1 + " and " + line2);

        System.out.println("Intersection is " + line2.intersect(line1));

        end.move(1.0, -5.0);
        System.out.println("Intersection is " + line1.intersect(line2));
    }
}
