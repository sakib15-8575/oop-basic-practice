package ChapterFive.NestedClass;

public class TryNestedClass {
    public static void main(String[] args) {
        System.out.println(new MagicHat("Gray Topper"));
        System.out.println(new MagicHat("Black Topper"));
        System.out.println(new MagicHat("Baseball Cap"));
        MagicHat oldHat = new MagicHat("Old hat");
        MagicHat.Rabbit rabbit = oldHat.new Rabbit();
        System.out.println(oldHat);
        System.out.println("\nNew rabbit is: " + rabbit);
    }
}
