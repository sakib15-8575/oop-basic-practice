package ChapterFive;

import static java.lang.Math.sqrt;

public class Point {
    private double x;
    private double y;

    public Point(double xVal, double yVal) {
        x = xVal;
        y = yVal;
    }

    public Point(final Point aPoint) {
        x = aPoint.x;
        y = aPoint.y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getY() {
        return y;
    }

    public void move(double xDelta, double yDelta) {
        x += xDelta;
        y += yDelta;
    }

    double distance(final Point aPoint) {
        return sqrt((x - aPoint.x)*(x - aPoint.x) + (y - aPoint.y)*(y - aPoint.y));
    }

    public String toString() {
        return Double.toString(x) + ", " + y;
    }
}
