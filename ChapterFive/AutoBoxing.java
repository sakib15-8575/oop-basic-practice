package ChapterFive;

public class AutoBoxing {
    public static void main(String[] args) {
        int[] values = {3, 97, 55, 22, 12345};

        Integer[] objs = new Integer[values.length];

        for (int i = 0; i < values.length; ++i) {
            objs[i] = boxInteger(values[i]);
        }

        for (Integer intObject : objs) {
            unboxInteger(intObject);
        }
    }

    public static Integer boxInteger(Integer obj) {
        return obj;
    }

    public static void unboxInteger(int n) {
        System.out.println("value = " + n);
    }
}
