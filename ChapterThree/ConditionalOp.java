package ChapterThree;

public class ConditionalOp {
    public static void main(String[] args) {
        int hatNum = 1;
        System.out.println("I have " + hatNum + " hat" + (hatNum == 1 ? "." : "s."));

        hatNum++;
        System.out.println("I have " + hatNum + " hat" + (hatNum == 1 ? "." : "s."));
    }
}
