package ChapterSeven;

public class TestTryCatch {
    public static void main(String[] args) {
        int i = 1;
        int j = 0;

        try {
            System.out.println("Try block entered " + "i = " + i + " j = " + j);
            System.out.println(i/j);
            System.out.println("Ending try block");

        }catch (ArithmeticException e) {
            System.out.println("Arithmetic Exception caught");
        }

        System.out.println("After try block");
    }
}
