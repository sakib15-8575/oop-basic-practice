package ChapterSix;

public class PetDog extends Dog{
    protected Flea petFlea;

    public PetDog(String name, String breed, String fleaName, String fleaSpecies) {
        super(name, breed);
        petFlea = new Flea("Max", "circus flea");
    }

    public PetDog(PetDog pet) {
        super(pet);
        petFlea = new Flea(pet.petFlea);
    }

    public Flea getFlea() {
        return petFlea;
    }

    public void sound() {
        System.out.println("Woof");
    }

    public String toString() {
        return super.toString() + " - a pet dog.\nIt has a flea:\n" + petFlea;
    }
}
