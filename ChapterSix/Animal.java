package ChapterSix;

public class Animal {
    private String type;

    public Animal(String aType) {
        type = new String(aType);
    }

    public Animal(Animal animal) {
        type = animal.type;
    }

    public void sound() {}

    public String toString() {
        return "This is a " + type;
    }
}
