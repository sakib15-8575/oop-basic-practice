package ChapterSix.LinkedListExample;

public class LinkedList {
    private ListItem start = null;
    private ListItem end = null;
    private ListItem current = null;

    private class ListItem {
        ListItem next;
        Object item;

        public ListItem(Object item) {
            this.item = item;
            next = null;
        }

        public String toString() {
            return "ListItem " + item;
        }
    }

    public LinkedList() {}

    public LinkedList(Object item) {
        if (item != null) {
            current = end = start = new ListItem(item);
        }
    }

    public LinkedList(Object[] items) {
        if (items != null) {
            for (Object item : items) {
                addItem(item);
            }
            current = start;
        }
    }

    public void addItem(Object item) {
        ListItem newEnd = new ListItem(item);
        if (start == null) {
            start = end = newEnd;
        }
        else {
            end.next = newEnd;
            end = newEnd;
        }
    }

    public Object getFirst() {
        current = start;
        return start == null ? null : start.item;
    }

    public Object getNext() {
        if (current != null) {
            current = current.next;
        }
        return current == null ? null : current.item;
    }
}
