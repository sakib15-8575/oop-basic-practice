package ChapterSix.LinkedListExample;

public class PolyLine {
    private LinkedList polyLine;

    public PolyLine(double[][] coords) {
        Point[] points = new Point[coords.length];

        for (int i = 0; i < coords.length; ++i) {
            points[i] = new Point(coords[i][0], coords[i][1]);
        }

        polyLine = new LinkedList(points);
    }

    public PolyLine(Point[] points) {
        polyLine = new LinkedList(points);
    }

    public void addPoint(Point point) {
        polyLine.addItem(point);
    }

    public void addPoint(double x, double y) {
        polyLine.addItem(new Point(x, y));
    }

    public String toString() {
        StringBuffer str = new StringBuffer("Polyline:");
        Point point = (Point) polyLine.getFirst();

        while (point != null) {
            str.append(" (" + point + ")");
            point = (Point) polyLine.getNext();
        }
        return str.toString();
    }
}
