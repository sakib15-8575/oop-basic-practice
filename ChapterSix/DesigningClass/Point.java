package ChapterSix.DesigningClass;

public class Point {
    protected double x;
    protected double y;

    public Point(double xVal, double yVal) {
        x = xVal;
        y = yVal;
    }

    public Point(Point point) {
        x = point.x;
        y = point.y;
    }

    public String toString() {
        return x + "," + y;
    }
}
