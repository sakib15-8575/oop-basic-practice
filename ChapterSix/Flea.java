package ChapterSix;

import ChapterThree.ForLoop;

public class Flea extends Animal{
    protected String name;
    protected String species;

    public Flea(String aName, String aSpecies) {
        super("Flea");
        name = aName;
        species = aSpecies;
    }

    public Flea(Flea flea) {
        super(flea);
        name = flea.name;
        species = flea.species;
    }

    public void setName(String aName) {
        name = aName;
    }

    public String getName() {
        return name;
    }

    public String getSpecies() {
        return species;
    }

    public void sound() {
        System.out.println("Psst");
    }

    public String toString() {
        return super.toString() + "\nIt's " + name + " the " + species;
    }
}
