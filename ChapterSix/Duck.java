package ChapterSix;

public class Duck extends Animal{
    protected String name;
    protected String breed;

    public Duck(String aName) {
        super("Duck");
        name = aName;
        breed = "Unknown";
    }

    public Duck(String aName, String aBreed) {
        super("Duck");
        name = aName;
        breed = aBreed;
    }

    public void layEgg() {
        System.out.println("Egg laid");
    }

    public String toString() {
        return super.toString() + "\nIt's " + name + " the " + breed;
    }

    public void sound() {
        System.out.println("Quack quackquack");
    }
}
